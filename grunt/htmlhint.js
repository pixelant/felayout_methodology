module.exports = {
    options: {
        htmlhintrc: '.htmlhintrc'
    },
    html: {
        src: ['<%= tmp %>/*.html']
    }
};
