module.exports = {
    html: {
        src: ['<%= tmp %>/*.html'],
        overwrite: true,
        replacements: [
            {
                from: '<link rel="stylesheet" href="bootstrap.css">',
                to: ''
            },
            {
                from: '<link rel="stylesheet" href="components.css">',
                to: ''
            }
        ]
    }
};
